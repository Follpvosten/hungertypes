# "Hunger Types"
Because I couldn't think of a better name.

This is supposed to be an implementation of a "Hunger Games"-like game in
TypeScript, and also my way of learning how to use stuff like npm.

It should be server-less software once it's deployed - it can be run locally
by just opening `index.html` in your preferred web browser.

Since this repository should mostly contain my code, most external libraries
and frameworks are installed using npm, and some symlinks are used because I
couldn't get some stuff to work without them (thanks, Fontawesome).

To install the dependencies, just run `npm install` in the project's root
directory, followed by something like `./node_modules/typescript/bin/tsc` to
compile the TypeScript code.  Then, you have to compile the provided
`custom.scss` file with your favorite sass compiler, to a file called
`bulma.css` in the `css` folder; for example: `sassc custom.scss css/bulma.css`

In theory, that should be enough to get everything going. Good luck!
