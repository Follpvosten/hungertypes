/* The character manager */
import * as Helpers from "./helpers.js";
import * as Dialogs from "./dialogs.js";
import * as Storage from "./storage.js";
import { ICharacter } from "./interfaces.js";
import { BulmaColor } from "./enums.js";

var selectedChars: number[];
var $exportSel: JQuery = null;
var $charList: JQuery = null;

function handleSelection($check: JQuery, fromCheck = false) {
    if (fromCheck) {
        if ($check.prop("checked")) {
            selectedChars.push($check.data("id"));
            $exportSel.prop("disabled", false);
        } else {
            selectedChars.splice(selectedChars.indexOf($check.data("id")), 1);
            if (selectedChars.length < 1)
                $exportSel.prop("disabled", true);
        }
    } else {
        if ($check.prop("checked")) {
            if (!fromCheck)
                $check.prop("checked", false);
            selectedChars.splice(selectedChars.indexOf($check.data("id")), 1);
            if (selectedChars.length < 1)
                $exportSel.prop("disabled", true);
        } else {
            if (!fromCheck)
                $check.prop("checked", true);
            selectedChars.push($check.data("id"));
            $exportSel.prop("disabled", false);
        }
    }
}

async function exportChars($modalMain: JQuery, all: boolean) {
    let charIds = all
        ? $modalMain.find("input").get().map(elem => Number(elem.dataset["id"]))
        : selectedChars;
    let characters = Storage.getAllCharacters().filter(c => charIds.includes(c.id));
    let question = "Really export all characters?";
    if (!all) {
        question = "Really export the following characters?<ul>";
        characters.forEach(c => question += `<li>${c.name}</li>`);
        question += "</ul>";
    }
    if (await Dialogs.confirm(question)) {
        characters = Storage.attachMetadata(characters);
        Helpers.generateDownload(JSON.stringify(characters), "characters.json");
        // Remove any selection
        $modalMain.find("input").prop("checked", false);
        selectedChars = [];
        $exportSel.prop("disabled", true);
    }
}

function importChars() {
    Helpers.selectJsonFile().then(file => {
        Helpers.readFileAsString(file).then(result => {
            Storage.processCharacterImport(JSON.parse(result))
                .then(renderCharList);
        }).catch(alert);
    }).catch();
}

async function deleteCharacter(char: ICharacter) {
    let question = `Do you really want to <strong>fully delete</strong> 
the character <strong>${char.name}</strong>?<br>
They will be removed from every game they have ever been in, and you can not 
get them back (unless you exported them first).`;
    if (await Dialogs.confirm(question, "Delete character", BulmaColor.Danger)) {
        if (await Dialogs.confirm("Are you sure?", "Delete character", BulmaColor.Danger)) {
            Storage.deleteCharacter(char.id);
            Storage.writeData();
            renderCharList();
        }
    }
}

function renderCharList() {
    $charList.empty();
    selectedChars = [];
    $exportSel.prop("disabled", true);
    let allCharacters = Storage.getAllCharacters();
    for (let char of allCharacters) {
        let $char = $(`
<div class="column is-one-third-desktop is-half-tablet">
  <div class="card">
    <div class="card-image">
      <figure class="image is-square">
        <img src="${char.image_url}" alt="Image">
      </figure>
    </div>
    <div class="card-content has-text-centered">
      <h5 class="title is-5">${char.name}</h5>
    </div>
    <footer class="card-footer">
      <a class="card-footer-item" data-action="edit">Edit</a>
      <a class="card-footer-item" data-action="select">
        <input type="checkbox" data-id="${char.id}">
      </a>
      <a class="card-footer-item has-text-danger" data-action="delete">Delete</a>
    </footer>
  </div>
</div>`);
        let $check = $char.find("input[type='checkbox']");
        $check.change(() => handleSelection($check, true));
        $char.find(".card-image,.card-content")
            .click(() => handleSelection($check));
        $char.find("[data-action='delete']")
            .click(() => deleteCharacter(char));

        $charList.append($char);
    }
}
export function showCharacterManager() {
    return new Promise(resolve => {
        let $modalMain = $(`
<div class="modal is-active">
  <div class="modal-background"></div>
  <div class="modal-card">
    <header class="modal-card-head">
      <p class="modal-card-title">Character manager</p>
    </header>
    <section class="modal-card-body">
      <div id="toolbar" class="buttons is-multiline">
        <button class="button" data-action="export-all">Export all</button>
        <button class="button" data-action="export-sel" disabled>Export selected</button>
        <button class="button is-success" data-action="add">
          <span class="icon is-small">
            <i class="fas fa-plus"></i>
          </span>
          <span>Add character</span>
        </button>
        <button class="button is-success" data-action="import">
          <span class="icon is-small">
            <i class="fas fa-upload"></i>
          </span>
          <span>Import character(s)</span>
        </button>
      </div>
      <div id="characters" class="columns is-mobile is-multiline">
      </div>
    </section>
    <footer class="modal-card-foot is-right">
      <button class="button" data-action="close">OK</button>
    </footer>
  </div>
</div>`);

        $modalMain.find("[data-action='export-all']")
            .click(() => exportChars($modalMain, true));
        $modalMain.find("[data-action='import']").click(importChars);
        $exportSel = $modalMain.find("[data-action='export-sel']");
        $exportSel.click(() => exportChars($modalMain, false));

        $charList = $modalMain.find("#characters");
        renderCharList();

        $modalMain.find("[data-action='close']").click(() => {
            resolve();
            $modalMain.remove();
        });
        $("body").append($modalMain);
    });
}
