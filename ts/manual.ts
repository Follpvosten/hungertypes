const checkedHtml = `<i class="fas fa-check-circle"></i>`;
const uncheckedHtml = `<i class="fas fa-circle"></i>`;

export var storageAvailable: boolean = false;

export function setup() {
    checkForStorage();
    $("#expl-next").click(explainCharacters);
}

function checkForStorage() {
    if (typeof Storage !== "undefined") {
        // Storage is available.
        storageAvailable = true;
        $("#check-storage").html(checkedHtml);
        $("#expl-content").html(
            `<h3 class="title">Storage is working!</h3>
<p class="is-size-6">The data you enter will be saved locally on your machine.
Nothing will be sent to anywhere else, and you should not lose any data.</p>`);
    } else {
        storageAvailable = false;
        $("#check-storage").html(`<i class="fas fa-check-times"></i>`);
        $("#expl-content").html(
            `<h3 class="title">Storage is unavailable!</h3>
<p class="is-size-6">It looks like your browser does not support local storage.
That means that your data will be lost as soon as you close or refresh the website.</p>`);
    }
}

function explainCharacters() {
    let $explNext = $("#expl-next").off("click");
    $("#arrow-storage").fadeOut(400);
    $("#arrow-characters").fadeIn(400, () => {
        $("#expl-content").html(
            `<h3 class="title">Enter data about your characters</h3>
<p class="is-size-6">Start with the name; then, add personality traits and images to make them unique!</p>
<p class="is-size-6">Each trait gives a character different abilities and changes the way they interact
with their opponents and the environment, giving them both advantages and disadvantages
during the battle.</p>
<p class="is-size-6">You can keep the default sorting of characters in numbered districts, but you
can also change that; for example, you could sort your characters into kingdoms,
races or houses.</p>`);
        $("#check-characters").html(checkedHtml);
        $explNext.click(explainStart);
    });
}

function explainStart() {
    let $explNext = $("#expl-next").off("click");
    $("#arrow-characters").fadeOut(400);
    $("#arrow-start").fadeIn(400, () => {
        $("#expl-content").html(
            `<h3 class="title">Start the battle!</h3>
<p class="is-size-6">When you're done entering character data, just press the Start button to
start the fight!</p>`);
        $("#check-start").html(checkedHtml);
        $("#expl-finished").show();
        $explNext.text("Start over").click(() => {
            $("#arrow-start,#expl-finished").hide();
            $("#arrow-storage").show();
            $explNext.text("Next step").click(explainCharacters);
            $("#check-characters,#check-start").html(uncheckedHtml);
            checkForStorage();
        });
    });
}
