import * as Manual from "./manual.js";
import * as Editor from "./editor.js";
import * as Storage from "./storage.js";

$(document).ready(() => {
    Manual.setup();
    // Set basic events
    $("#get-started").click(() => {
        $("html").animate({
            scrollTop: $("#manual").offset().top
        }, 500);
    });
    $("#expl-finished,#start-now").click(() => {
        $("html").animate({
            scrollTop: $("#characters").offset().top
        }, 300);
    });
    Storage.setup(Manual.storageAvailable);
    Editor.setup($("#editor"));
});
