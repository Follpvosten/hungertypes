/* Helper functions for various things */

export function generateDownload(data: string, filename: string) {
    let a = document.createElement("a");
    let blob = new Blob([data], { type: "octet/stream" });
    let url = window.URL.createObjectURL(blob);
    a.setAttribute("href", url);
    a.setAttribute("download", filename);
    a.style.display = "none";
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
    window.URL.revokeObjectURL(url);
}

export function selectJsonFile() {
    return new Promise<File>((resolve, reject) => {
        let $input = $(`<input type="file" accept=".json">`);
        let handler = () => {
            let file = (<HTMLInputElement>$input[0]).files[0];
            if (file)
                resolve(file);
            else
                reject();
            $input.remove();
            $(document).off("mousemove.sfile touchstart.sfile keypress.sfile");
            $(window).off("scroll.sfile");
        };
        $input.hide().change(handler)
            .appendTo($("body")).click();
        $(document).on("mousemove.sfile touchstart.sfile keypress.sfile", handler);
        $(window).on("scroll.sfile", handler);
    });
}

export function readFileAsString(file: File) {
    return new Promise<string>((resolve, reject) => {
        let reader = new FileReader();
        reader.onload = () => {
            resolve(reader.result);
        };
        reader.onerror = reader.onabort = () => {
            reject(reader.error);
        };
        reader.readAsText(file);
    });
}
