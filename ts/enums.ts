/* Simple enums */

export enum Gender {
    Male = 0,
    Female = 1,
    TransMTF = 2,
    TransFTM = 3,
    Robot = 4,
    Cheese = 5
}

export enum BulmaColor {
    Success = "is-success",
    Danger = "is-danger",
    Warning = "is-warning",
    Primary = "is-primary",
    Info = "is-info",
    Link = "is-link",
    White = "is-white",
    Light = "is-light",
    Dark = "is-dark",
    Black = "is-black"
}
