/* Helper functions and dialogs. */
import * as Editor from "./editor.js";
import * as Storage from "./storage.js";
import { Traits } from "./traits.js";
import { ICharacter, IDistrict, IGame } from "./interfaces.js";
import { BulmaColor } from "./enums.js";

export function confirm(question: string, title = "Question", yesColor = BulmaColor.Success) {
    return new Promise(resolve => {
        let $modal = $(`
<div class="modal is-active">
  <div class="modal-background"></div>
  <div class="modal-card">
    <header class="modal-card-head">
      <p class="modal-card-title">${title}</p>
    </header>
    <section class="modal-card-body">
      <div class="content">${question}</div>
    </section>
    <footer class="modal-card-foot is-right">
      <button class="button no-button">No</button>
      <button class="button yes-button ${yesColor}">Yes</button>
    </footer>
  </div>
</div>`);
        $modal.find(".no-button").click(() => {
            $modal.remove();
            resolve(false);
        });
        $modal.find(".yes-button").click(() => {
            $modal.remove();
            resolve(true);
        });
        $("body").append($modal);
    });
}

export function getImageUrl(originalUrl: string) {
    return new Promise<string>(resolve => {
        let $modal = $(`
<div class="modal is-active">
  <div class="modal-background"></div>
  <div class="modal-card">
    <header class="modal-card-head">
      <p class="modal-card-title">Select an image</p>
    </header>
    <section class="modal-card-body">
      <label class="label">Image URL</label>
      <div class="field has-addons">
        <div class="control is-expanded">
          <input type="text" class="input">
        </div>
        <div class="control">
          <button class="button load-button">
    	    <span class="icon is-small"><i class="fas fa-sync"></i></span>
    	    <span>Load image</span>
          </button>
        </div>
      </div>
      <label class="label">Preview</label>
      <figure class="image is-96x96">
        <img src="img/no-image.png">
      </figure>
    </section>
    <footer class="modal-card-foot is-right">
      <button class="button cancel-button">Cancel</button>
      <button class="button save-button is-success">Save image</button>
    </footer>
  </div>
</div>`);
        let $input = $modal.find("input").val(originalUrl);

        $modal.find(".load-button").click(() => {
            $modal.find("img").attr("src", $input.val());
        });

        $modal.find(".cancel-button").click(() => {
            $modal.remove();
            resolve(null);
        });
        $modal.find(".save-button").click(() => {
            $modal.remove();
            resolve($input.val());
        });

        $("body").append($modal);
    });
}

export function chooseCharacter(excludes: number[] = null) {
    return new Promise<ICharacter>(resolve => {
        let $modal = $(`
<div class="modal is-active">
  <div class="modal-background"></div>
  <div class="modal-card">
    <header class="modal-card-head">
      <p class="modal-card-title">Choose a character</p>
    </header>
    <section class="modal-card-body">
      <div class="field">
        <label class="label">Search</label>
        <div class="control">
          <input type="text" class="input" placeholder="Enter filter...">
        </div>
      </div>
      <div class="box">
        <div id="characters" class="columns is-multiline"></div>
      </div>
    </section>
    <footer class="modal-card-foot is-right">
      <button class="button">Cancel</button>
    </footer>
  </div>
</div>`);

        let $body = $modal.find("#characters");
        let showChars = (filter: string) => {
            $body.empty();
            let characters = Storage.getAllCharacters();
            if (excludes)
                characters = characters.filter(c => !excludes.includes(c.id));
            if (filter.length > 0)
                characters = characters.filter(c => c.name.toLowerCase().includes(filter.toLowerCase()));
            characters.forEach(character => {
                let $character = $(`
<div class="column is-one-third">
  <div class="card">
    <div class="card-image">
      <figure class="image is-square">
        <img src="${character.image_url}" alt="Image">
      </figure>
    </div>
    <div class="card-content has-text-centered">
      <h5 class="title is-5">${character.name}</h5>
    </div>
  </div>
</div>`);

                $character.click(() => {
                    $modal.remove();
                    resolve(character);
                });
                $body.append($character);
            });
        }

        let $filter = $modal.find("input");
        $filter.keyup(() => showChars($filter.val()));
        showChars("");

        $modal.find("button").click(() => {
            $modal.remove();
            resolve(null);
        });

        $("body").append($modal);
    });
}

export function selectGame(exclude: number) {
    return new Promise<number>(resolve => {
        let $modal = $(`
<div class="modal is-active">
  <div class="modal-background"></div>
  <div class="modal-card">
    <header class="modal-card-head">
      <p class="modal-card-title">Select game to load</p>
      <button class="delete" data-action="close"></button>
    </header>
    <section class="modal-card-body">
      <div class="field">
        <label class="label">Search</label>
        <div class="control">
          <input type="text" class="input" placeholder="Enter filter...">
        </div>
      </div>
      <div id="games">
      </div>
    </section>
    <footer class="modal-card-foot is-right">
      <button class="button" data-action="close">Cancel</button>
    </footer>
</div>`);

        $modal.find("[data-action='close']").click(() => {
            $modal.remove();
            resolve(null);
        });

        let $games = $modal.find("#games");
        let $input = $modal.find("input");
        let showGames = (filter: string) => {
            $games.empty();
            let games = Storage.getAllGames().filter(g => g.id != exclude);
            if (filter.length > 0)
                games = games.filter(g => g.name.toLowerCase().includes(filter.toLowerCase()));
            games.forEach(game => {
                let namingText = game.standard_naming ? "Default naming" : "Custom naming";
                let $game = $(`
<div class="notification is-primary">
  <nav class="level is-mobile">
    <div class="level-item">
      <h4 class="title is-4">${game.name}</h4>
    </div>
    <div class="level-item">
      <h5 class="subtitle is-5">${game.districts.length} Districts</h5>
    </div>
    <div class="level-item">
      <h5 class="subtitle is-5">${namingText}</h5>
    </div>
  </nav>
</div>`);
                $game.click(() => {
                    $modal.remove();
                    resolve(game.id);
                });
                $games.append($game);
            });
        };
        $input.keyup(() => showGames($input.val()));
        showGames("");

        $("body").append($modal);
    });
}

export function showTraitDialog(character: ICharacter, color: BulmaColor) {
    return new Promise(resolve => {
        let $modal = $(`
<div class="modal is-active">
  <div class="modal-background" data-action="close"></div>
  <div class="modal-card">
    <header class="modal-card-head">
      <p class="modal-card-title">Add traits</p>
      <button class="delete" data-action="close"></button>
    </header>
    <section class="modal-card-body">
      <div class="field">
        <label class="label">Filter</label>
        <div class="control">
          <input type="text" class="input" placeholder="Enter search text">
        </div>
      </div>
      <div class="box">
        <div id="traits" class="field is-grouped is-grouped-multiline"></div>
      </div>
    </section>
    <footer class="modal-card-foot is-right">
      <button class="button" data-action="close">OK</button>
    </footer>
  </div>
</div>`);

        let $traits = $modal.find("#traits");
        let $filter = $modal.find("input");
        let showTraits = (filter: string) => {
            $traits.empty();
            let traits = Traits.filter(t => !character.traits.map(t => t.traitId).includes(t.id) || t.hasTargets);
            if (filter.length > 0)
                traits = traits.filter(t => t.name.startsWith(filter) || t.description.includes(filter));
            traits.forEach(trait => {
                let $trait = $(`
<div class="control">
  <div class="tags">
    <a class="tag ${color}">${trait.name}</a>
  </div>
</div>`);
                $trait.find(".tag").click(async () => {
                    let question = `<p>Really add this trait to ${character.name}?</p>
<p>Description: ${trait.description}</p>`;
                    if (await confirm(question, "Add trait")) {
                        if (trait.hasTargets) {
                            chooseCharacter().then(target => {
                                if (target) {
                                    character.traits.push({
                                        traitId: trait.id,
                                        targets: [target.id]
                                    });
                                }
                            });
                        } else {
                            character.traits.push({ traitId: trait.id });
                            $trait.fadeOut(400, () => showTraits($filter.val()));
                        }
                    }
                });
                $traits.append($trait);
            });
        }

        $filter.keyup(() => showTraits($filter.val()));
        showTraits("");

        $modal.find("[data-action='close']").click(() => {
            $modal.remove();
            resolve();
        });

        $("body").append($modal);
    });
}

export function showDistrictSettings(district: IDistrict) {
    return new Promise<IDistrict>(resolve => {
        const colors: BulmaColor[] = [
            BulmaColor.Black,
            BulmaColor.Primary,
            BulmaColor.Success,
            BulmaColor.Warning,
            BulmaColor.Info,
            BulmaColor.Link,
            BulmaColor.Danger
        ];
        let result = district;
        let $modal = $(`
<div class="modal is-active">
  <div class="modal-background"></div>
  <div class="modal-card">
    <header class="modal-card-head">
      <p class="modal-card-title">District settings</p>
    </header>
    <section class="modal-card-body">
      <div class="field">
        <label class="label">Name</label>
        <div class="control">
          <input type="text" class="input">
        </div>
      </div>
      <div class="field">
        <label class="label">Color</label>
        <div id="colors" class="field is-grouped is-grouped-multiline">
        </div>
      </div>
    </section>
    <footer class="modal-card-foot is-right">
      <button class="button" data-action="cancel">Cancel</button>
      <button class="button is-success" data-action="save">Save</button>
    </footer>
  </div>
</div>`);
        let $input = $modal.find("input").val(district.name);

        let $colors = $modal.find("#colors");
        colors.forEach(color => {
            let $button = $(`<button class="button ${color}">&nbsp;&nbsp;&nbsp;&nbsp;</button>`);
            $button.click(() => {
                $colors.find("button.is-active").removeClass("is-active");
                result.color = color;
                $button.addClass("is-active");
            });
            $colors.append($(`<p class="control">`).append($button));
        });
        $colors.find(`button.${result.color}`).addClass("is-active");

        $modal.find("[data-action='cancel']").click(() => {
            $modal.remove();
            resolve(null);
        });
        $modal.find("[data-action='save']").click(() => {
            result.name = $input.val();
            $modal.remove();
            resolve(result);
        });

        $("body").append($modal);
    });
}

export function showGameSettings(game: IGame) {
    return new Promise<IGame>(resolve => {
        let checked = game.standard_naming ? "checked" : "";
        let $modal = $(`
<div class="modal is-active">
  <div class="modal-background"></div>
  <div class="modal-card">
    <header class="modal-card-head">
      <p class="modal-card-title">Game settings</p>
      <button class="delete" data-action="close"></button>
    </header>
    <section class="modal-card-body">
      <div class="field">
        <label class="label">Name</label>
        <div class="control">
          <input type="text" class="input">
        </div>
      </div>
      <label class="checkbox">
        <input type="checkbox" ${checked}>
        Use default naming scheme
      </label>
    </section>
    <footer class="modal-card-foot is-right">
      <button class="button" data-action="close">Cancel</button>
      <button class="button is-success" data-action="save">Save</button>
    </footer>
  </div>
</div>`);

        $modal.find("[data-action='close']").click(() => {
            $modal.remove();
            resolve(null);
        });

        let $name = $modal.find("input[type='text']");
        $name.val(game.name);
        let $naming = $modal.find("input[type='checkbox']");
        let result = game;

        $modal.find("[data-action='save']").click(() => {
            result.name = $name.val();
            result.standard_naming = $naming.prop("checked");
            $modal.remove();
            resolve(result);
        });

        $("body").append($modal);
    });
}
