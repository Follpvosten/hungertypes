/* Base interfaces */
import { Gender, BulmaColor } from "./enums";

export interface IGame {
    id: number;
    name: string;
    standard_naming: boolean;
    districts: number[];
}

export interface ISavedGame {
    game: IGame;
    characters: ICharacter[];
    districts: IDistrict[];
}

export interface ICharacter {
    id: number;
    name: string;
    guid?: string;
    age: number;
    gender: Gender | string;
    image_url: string;
    traits: ITraitData[];
}

export interface IDistrict {
    id: number;
    name: string;
    color: BulmaColor;
    characters: number[];
}

export interface ITrait {
    id: number;
    name: string;
    description: string;
    hasTargets: boolean;
    displayName?: (traitData: ITraitData) => string;
    killModifier: (target: ICharacter,
        self: ICharacter,
        traitData: ITraitData) => number;
}

/**
 * Used to hold data for a specific trait, for a specific user.
 */
export interface ITraitData {
    traitId: number;
    targets?: number[];
}
