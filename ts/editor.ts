/* The character editor/game setup. */
import { IGame, ISavedGame } from "./interfaces";
import { Character, District } from "./classes.js";
import { BulmaColor } from "./enums.js";
import * as Storage from "./storage.js";
import * as Helpers from "./helpers.js";
import * as Dialogs from "./dialogs.js";
import { showCharacterManager } from "./charman.js";

var currentGame: IGame;

export var currentDistricts: District[];
export var currentChars: Character[];

var $contentMain: JQuery;

export function setup(elem: JQuery) {
    $contentMain = elem;
    // The "File" menu
    $("#save-game").click(saveGame);
    $("#load-game").click(selectGame);
    $("#save-file").click(saveFile);
    $("#load-file").click(loadFile);
    $("#show-file-menu").click(() => {
        $("#file-menu").toggleClass("is-active");
        $("#game-menu").removeClass("is-active");
    });
    // The "Game" menu
    $("#new-game").click(newGame);
    $("#delete-game").click(deleteGame);
    $("#game-settings").click(() => {
        $("#game-menu").removeClass("is-active");
        Dialogs.showGameSettings(currentGame).then(result => {
            if (result) {
                currentGame = result;
                saveGame();
            }
        });
    });
    $("#show-game-menu").click(() => {
        $("#game-menu").toggleClass("is-active");
        $("#file-menu").removeClass("is-active");
    });
    // Charman
    $("#manage-characters").click(() => {
        showCharacterManager().then(() => {
            // Reload the current game to show changes
            loadGame(Storage.getGameData(currentGame.id));
        });
    });
    // Close menus when clicking somewhere else in the editor
    $contentMain.click(() => $("#file-menu,#game-menu").removeClass("is-active"));
    loadGame(Storage.getLastGame());
}

function loadGame(gameData: ISavedGame) {
    currentGame = gameData.game;
    currentDistricts = gameData.districts.map(d => new District(d));
    currentChars = gameData.characters.map(c => new Character(c));
    rerender();
}
export function saveGame() {
    currentChars.forEach(Storage.saveCharacter);
    currentDistricts.forEach(Storage.saveDistrict);
    Storage.saveGame(currentGame);
    Storage.writeData();
    $("#file-menu").removeClass("is-active");
}

function selectGame() {
    Dialogs.selectGame(currentGame.id).then(id => {
        if (id)
            loadGame(Storage.getGameData(id));
    });
    $("#file-menu").removeClass("is-active");
}

function saveFile() {
    let data: ISavedGame = {
        game: currentGame,
        characters: Storage.attachMetadata(currentChars),
        districts: currentDistricts
    };
    Helpers.generateDownload(JSON.stringify(data), currentGame.name + ".json");
    $("#file-menu").removeClass("is-active");
}
function loadFile() {
    $("#file-menu").removeClass("is-active");
    Helpers.selectJsonFile().then(file => {
        Helpers.readFileAsString(file).then(result => {
            saveGame();
            Storage.processImport(JSON.parse(result)).then(newGameId => {
                loadGame(Storage.getGameData(newGameId));
            }).catch(alert);
        }).catch(alert);
    }).catch(); // Just do nothing when no file is selected.
}

function newGame() {
    $("#game-menu").removeClass("is-active");
    // Save the current game and create a new one
    saveGame();
    let newId = Storage.getNextGameId();
    let newGame: IGame = {
        id: newId,
        name: "New game #" + newId,
        standard_naming: true,
        districts: []
    };
    Storage.saveNewGame(newGame);
    loadGame({
        game: newGame,
        districts: [],
        characters: []
    });
}
async function deleteGame() {
    $("#game-menu").removeClass("is-active");
    let question = `Do you really want to fully delete "<strong>${currentGame.name}</strong>"?<br>
All district settings will be lost, but characters will remain for selection.`;
    if (await Dialogs.confirm(question, "Delete game", BulmaColor.Danger)) {
        if (await Dialogs.confirm("Are you sure?", "Delete game", BulmaColor.Danger)) {
            currentGame.districts.forEach(Storage.deleteDistrict);
            Storage.deleteGame(currentGame.id);
            Storage.writeData();
            loadGame(Storage.getLastGame());
        }
    }
}

export function rerender() {
    $contentMain.empty();
    currentDistricts.forEach(d => $contentMain.append(d.toElement()));
    let $addDistrict = $(`
<div class="column is-half-desktop">
  <div class="message is-success">
    <div class="message-header">
      <p>Add district</p>
    </div>
    <div class="message-body has-text-centered">
      <button class="button is-large is-success">
        <span class="icon is-large">
          <i class="fas fa-2x fa-plus"></i>
        </span>
      </button>
    </div>
  </div>
</div>`);
    $addDistrict.find("button").click(() => {
        let newId = Storage.getNextDistrictId();
        let name = currentGame.standard_naming ? "District " + (currentDistricts.length + 1) : "Something";
        let newDistrict = new District({
            id: newId,
            name: name,
            color: BulmaColor.Success,
            characters: []
        });
        Storage.saveNewDistrict(newDistrict);
        currentDistricts.push(newDistrict);
        currentGame.districts.push(newId);
        let $newDistrict = newDistrict.toElement().hide();
        $newDistrict.insertBefore($addDistrict).fadeIn(400, rerenderAndSave);
    });
    $contentMain.append($addDistrict);
}
export function rerenderAndSave() {
    rerender();
    saveGame();
}

export function removeDistrict(district: District) {
    // Delete the district's characters from the current game...
    district.characters.forEach(charId => {
        currentChars.splice(
            currentChars.findIndex(c => c.id == charId), 1
        );
    });
    // And remove all traces of the district
    currentGame.districts.splice(
        currentGame.districts.indexOf(district.id), 1
    );
    currentDistricts.splice(
        currentDistricts.indexOf(district), 1
    );
    Storage.deleteDistrict(district.id);
}

export function removeCharacter(character: Character, district: District) {
    district.characters.splice(
        district.characters.indexOf(character.id), 1
    );
    currentChars.splice(
        currentChars.indexOf(character), 1
    );
}
