/* This module manages the local storage. */
import { IGame, ICharacter, IDistrict, ISavedGame } from "./interfaces";
import { BulmaColor } from "./enums.js";
import * as Dialogs from "./dialogs.js";

var games: IGame[] = [];
var characters: ICharacter[] = [];
var districts: IDistrict[] = [];

var lastLoadedGame: number = null;

var useStorage: boolean;

export function setup(storageAvailable: boolean) {
    useStorage = storageAvailable;
    // Load data if available
    if (useStorage) {
        if (localStorage.games)
            games = JSON.parse(localStorage.games);
        if (localStorage.all_characters)
            characters = JSON.parse(localStorage.all_characters);
        if (localStorage.all_districts)
            districts = JSON.parse(localStorage.all_districts);
        if (localStorage.last_game)
            lastLoadedGame = localStorage.last_game;
    }
    validateData();

    if (games.length < 1) {
        // Create a first game.
        districts.push({
            id: 1,
            name: "District 1",
            color: BulmaColor.Success,
            characters: []
        });
        games.push({
            id: 1,
            name: "First game",
            standard_naming: true,
            districts: [1]
        });
    }
}

export function getLastGame(): ISavedGame {
    if (lastLoadedGame && games.some(g => g.id == lastLoadedGame)) {
        return getGameData(lastLoadedGame);
    } else {
        return getGameData(games[games.length - 1].id);
    }
}

export function getGameData(id: number): ISavedGame {
    let game = getGame(id);
    let gameDistricts = districts.filter(d => game.districts.includes(d.id));
    let gameCharacters: ICharacter[] = [];
    gameDistricts.forEach(dist => dist.characters.forEach(charId => {
        gameCharacters.push(getCharacter(charId));
    }));
    lastLoadedGame = id;
    writeData();
    return {
        game: game,
        districts: gameDistricts,
        characters: gameCharacters
    };
}

export function getGame(id: number) {
    return games.find(g => g.id == id);
}
export function getCharacter(id: number) {
    return characters.find(c => c.id == id);
}
export function getAllGames() {
    return games;
}
export function getAllCharacters() {
    return characters;
}

export function saveNewGame(game: IGame) {
    games.push(game);
}
export function saveNewDistrict(district: IDistrict) {
    districts.push(district);
}
export function saveNewCharacter(character: ICharacter) {
    characters.push(character);
}

export function saveGame(game: IGame) {
    let oldGame = games.find(g => g.id == game.id);
    if (!oldGame)
        saveNewGame(game);
    else
        games[games.indexOf(oldGame)] = game;
    lastLoadedGame = game.id;
}
export function saveDistrict(district: IDistrict) {
    let oldDist = districts.find(d => d.id == district.id);
    if (!oldDist)
        saveNewDistrict(district);
    else
        districts[districts.indexOf(oldDist)] = district;
}
export function saveCharacter(character: ICharacter) {
    let oldChar = characters.find(c => c.id == character.id);
    if (!oldChar)
        saveNewCharacter(character);
    else
        characters[characters.indexOf(oldChar)] = character;
}

export function deleteDistrict(id: number) {
    let dist = districts.find(d => d.id == id);
    if (dist)
        districts.splice(districts.indexOf(dist), 1);
}
export function deleteGame(id: number) {
    let game = games.find(g => g.id == id);
    if (game) {
        games.splice(games.indexOf(game), 1);
        if (lastLoadedGame == game.id)
            lastLoadedGame = null;
        if (games.length < 1) {
            // Create a first game.
            districts.push({
                id: 1,
                name: "District 1",
                color: BulmaColor.Success,
                characters: []
            });
            games.push({
                id: 1,
                name: "First game",
                standard_naming: true,
                districts: [1]
            });
        }
    }
}
export function deleteCharacter(id: number) {
    let char = characters.find(c => c.id == id);
    if (char) {
        characters.splice(characters.indexOf(char), 1);
        // Remove the character from all districts
        districts.forEach(dist => {
            if (dist.characters.includes(char.id)) {
                dist.characters.splice(dist.characters.indexOf(char.id), 1);
                saveDistrict(dist);
            }
        });
    }
}

export function getNextGameId() {
    return games.concat()
        .sort((a, b) => a.id < b.id ? 1 : -1)[0].id + 1;
}
export function getNextDistrictId() {
    return districts.concat()
        .sort((a, b) => a.id < b.id ? 1 : -1)[0].id + 1;
}
export function getNextCharacterId() {
    if (characters.length < 1) return 1;
    return characters.concat()
        .sort((a, b) => a.id < b.id ? 1 : -1)[0].id + 1;
}

/**
 * Loops through the loaded data and removes inconsistencies.
 */
function validateData() {
    // Only do anything if there is data.
    if (games.length > 0) {
        // There are saved games.
        // Check for data consistency
        games.forEach(game => game.districts.forEach(distId => {
            let district = districts.find(d => d.id == distId);
            if (!district) {
                // District doesn't exist. Remove inconsistent data.
                game.districts.splice(game.districts.indexOf(distId), 1);
            } else {
                // District exists. Validate characters.
                district.characters.forEach(charId => {
                    let character = characters.find(c => c.id == charId);
                    if (!character) {
                        // Character doesn't exist. Remove inconsistent data.
                        district.characters.splice(district.characters.indexOf(charId), 1);
                    }
                });
            }
        }));
        writeData();
    }
}

export function writeData() {
    if (useStorage) {
        // Exclude characters without a name
        characters = characters.filter(c => c.name.length > 0);

        localStorage.games = JSON.stringify(games);
        localStorage.all_districts = JSON.stringify(districts);
        localStorage.all_characters = JSON.stringify(characters);
        if (lastLoadedGame)
            localStorage.last_game = lastLoadedGame;
    }
}

function newGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

export function attachMetadata(data: ICharacter[]) {
    // For each character, attach a pseudo-guid if they don't have one yet
    data.forEach(char => {
        if (!char.guid) {
            char.guid = newGuid();
            saveCharacter(char);
        }
    });
    // ...and save it in the on-disk database
    writeData();
    return data;
}

export async function processImport(data: ISavedGame): Promise<number> {
    return new Promise<number>(async (resolve, reject) => {
        // A little consistency check
        if (!data.game) {
            reject("Incomplete data!");
        }
        // Because the imported data might be from a completely different system,
        // we need to re-assign all of the IDs in the dataset.
        // The game references its districts.
        data.game.id = getNextGameId();
        for (let id of data.game.districts) {
            let index = data.game.districts.indexOf(id);
            let newId = getNextDistrictId();
            let dist = data.districts.find(d => d.id == id);
            dist.id = newId;
            data.game.districts[index] = newId;
            // The district references its characters
            for (let charId of dist.characters) {
                let charIndex = dist.characters.indexOf(charId);
                // This gives us a reference to the array entry
                let char = data.characters.find(c => c.id == charId);
                // So we have to copy it.
                let newChar: ICharacter = {
                    id: char.id,
                    name: char.name,
                    guid: char.guid,
                    age: char.age,
                    gender: char.gender,
                    image_url: char.image_url,
                    traits: char.traits
                };
                let oldChar = characters.find(c => c.guid == char.guid);
                if (oldChar) {
                    let question = `The character <strong>${char.name}</strong>
already exists in the local database (with the name <strong>${oldChar.name}</strong>).<br>
Replace it with <strong>${char.name}</strong> from the loaded file?`;
                    if (await Dialogs.confirm(question)) {
                        newChar.id = oldChar.id;
                        dist.characters[charIndex] = oldChar.id;
                        saveCharacter(newChar);
                    }
                    else {
                        newChar.id = getNextCharacterId();
                        dist.characters[charIndex] = newChar.id;
                        saveNewCharacter(newChar);
                    }
                } else {
                    newChar.id = getNextCharacterId();
                    dist.characters[charIndex] = newChar.id;
                    saveNewCharacter(newChar);
                }
            }
            saveNewDistrict(dist);
        }
        saveNewGame(data.game);
        writeData();
        resolve(data.game.id);
    });
}

export async function processCharacterImport(data: ICharacter[]) {
    return new Promise(async (resolve) => {
        for (let char of data) {
            let oldChar = characters.find(c => c.guid == char.guid);
            if (oldChar) {
                let question = `The character "<strong>${char.name}</strong>"
already exists in the local database (with the name <strong>${oldChar.name}</strong>).<br>
Replace it with <strong>${char.name}</strong> from the loaded file?`;
                if (await Dialogs.confirm(question)) {
                    char.id = oldChar.id;
                    saveCharacter(char);
                } else {
                    char.id = getNextCharacterId();
                    saveNewCharacter(char);
                }
            } else {
                char.id = getNextCharacterId();
                saveNewCharacter(char);
            }
        }
        writeData();
        resolve();
    });
}
