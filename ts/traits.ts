/* These are the base traits I have defined for now. */
import { ITrait } from "./interfaces";
import * as Editor from "./editor.js";
import * as Storage from "./storage.js";

export var Traits: ITrait[] = [{
    id: 1,
    name: "Gay",
    description: "Less likely to kill characters of the same gender.",
    hasTargets: false,
    killModifier: (target, self): number => {
        return self.gender === target.gender ? 0.5 : 1;
    }
}, {
    id: 2,
    name: "Straight",
    description: "Less likely to kill characters of other genders.",
    hasTargets: false,
    killModifier: (target, self): number => {
        return self.gender === target.gender ? 1 : 0.5;
    }
}, {
    id: 3,
    name: "Loves ...",
    description: "Is much less likely to kill a specific person.",
    hasTargets: true,
    displayName: (traitData) => {
        let name = Storage.getCharacter(traitData.targets[0]).name;
        if (name.length < 1)
            name = "Unnamed character";
        return `Loves ${name}`;
    },
    killModifier: (target, self, traitData): number => {
        return target.id == traitData.targets[0] ? 0.25 : 1;
    }
}, {
    id: 4,
    name: "Psychopath",
    description: "Might kill anyone at random.",
    hasTargets: false,
    killModifier: Math.random // lol
}, {
    id: 5,
    name: "Lazy",
    description: "Tries to do nothing whenever they can.",
    hasTargets: false,
    killModifier: () => 0.1
}, {
    id: 6,
    name: "Strong",
    description: "Defeats other people more easily and is less easier to defeat.",
    hasTargets: false,
    killModifier: () => 1
}];
