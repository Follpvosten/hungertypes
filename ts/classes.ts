/* Classes for hungertypes */
import * as Editor from "./editor.js";
import * as Dialogs from "./dialogs.js";
import * as Storage from "./storage.js";
import { ICharacter, IDistrict, ITraitData } from "./interfaces.js";
import { Traits } from "./traits.js";
import { Gender, BulmaColor } from "./enums.js";

export class Character implements ICharacter {
    id: number;
    name: string;
    age: number;
    gender: Gender | string;
    image_url: string;
    traits: ITraitData[];

    constructor(source: ICharacter = null) {
        if (source) {
            Object.keys(source).forEach(key => this[key] = source[key]);
        }
    }

    protected createGenderSelect() {
        let $result = $(`
	  <div class="control is-expanded">
	    <div class="select is-fullwidth">
	      <select>
                <option value="0">Male</option>
	  	<option value="1">Female</option>
	  	<option value="2">Transgendered-MTF</option>
	  	<option value="3">Transgendered-FTM</option>
	  	<option value="4">Robot</option>
	  	<option value="5">Cheese</option>
                <option value="6">Custom...</option>
	      </select>
	    </div>
          </div>`);

        let $select = $result.find("select");
        $select.change(() => {
            if ($select.val() == 6) {
                this.gender = "";
                $result.replaceWith(this.createGenderInput());
            } else {
                this.gender = Number($select.val());
            }
        });
        $select.find(`[value='${this.gender}']`).prop("selected", true);
        return $result;
    }

    protected createGenderInput() {
        let $result = $(`
	  <div class="field has-addons">
	    <div class="control">
	      <button class="button" title="Back to defaults">
	  	<span class="icon is-small"><i class="fas fa-arrow-left"></i></span>
	      </button>
	    </div>
	    <div class="control is-expanded">
	      <input type="text" class="input" placeholder="Enter gender">
	    </div>
	  </div>`);

        let $input = $result.find("input").val(this.gender);
        $input.keyup(() => {
            this.gender = $input.val();
        });

        $result.find("button").click(() => {
            this.gender = Gender.Cheese;
            $result.replaceWith(this.createGenderSelect());
        });
        return $result;
    }

    protected createGenderControl() {
        if (typeof this.gender === "string") {
            return this.createGenderInput();
        } else {
            return this.createGenderSelect();
        }
    }

    protected createTraitsControl() {
        let myDistrict = Editor.currentDistricts
            .find(d => d.characters.includes(this.id));
        let $result = $(`<div class="field is-grouped is-grouped-multiline">`);

        this.traits.forEach(data => {
            let trait = Traits.find(t => t.id == data.traitId);
            let traitName = trait.displayName ? trait.displayName(data) : trait.name;
            let $trait = $(`
<div class="control">
  <div class="tags has-addons">
    <span class="tag ${myDistrict.color}">${traitName}</span>
    <a class="tag is-delete"></a>
  </div>
</div>`);
            $trait.find("a").click(async () => {
                if (await Dialogs.confirm(`Really remove trait <strong>${traitName}</strong> from <strong>${this.name}</strong>?`)) {
                    this.traits.splice(this.traits.indexOf(data), 1);
                    $trait.fadeOut(400, Editor.rerenderAndSave);
                }
            });
            $result.append($trait);
        });
        let $addTrait = $(`
<div class="control">
  <div class="tags">
    <a class="tag ${myDistrict.color}">
      <span class="icon is-small"><i class="fas fa-plus"></i></span>
    </a>
  </div>
</div>`);
        $addTrait.find("a").click(() => {
            Dialogs.showTraitDialog(this, myDistrict.color).then(Editor.rerenderAndSave);
        });
        $result.append($addTrait);

        return $(`<div class="box">`).append($result);
    }

    protected createNameInput() {
        let $input = $(
            `<input type="text" class="input" placeholder="Enter name">`
        ).val(this.name);
        let changeHandle = () => {
            this.name = $input.val().trim();
            $input.replaceWith(this.createNameTitle());
            Editor.rerenderAndSave();
        };
        $input.blur(changeHandle).keypress(e => {
            if (e.keyCode == 13)
                changeHandle();
        });
        return $input;
    }

    protected createNameTitle() {
        let title = this.name.length > 0 ? this.name : "Click to enter name";
        let $title = $(`
<p>
  ${title}
  <span class="icon is-small">
    <i class="fas fa-edit" title="Edit name..."></i>
  </span>
</p>`);
        $title.click(() => {
            let $input = this.createNameInput();
            $title.replaceWith($input);
            $input.focus();
        });
        return $title;
    }

    toElement(): JQuery {
        // Get the district we're in
        let myDistrict = Editor.currentDistricts
            .find(d => d.characters.includes(this.id));
        let $result = $(`<div class="message ${myDistrict.color}">`);

        let $header = $(`<div class="message-header">`);
        $header.append(this.createNameTitle());
        let $delete = $(`<button class="delete"></button>`);
        $delete.click(async () => {
            // Ask for confirmation
            let name = this.name.length > 0 ? this.name : "Unnamed character";
            if (await Dialogs.confirm(`Really remove <strong>${name}</strong> from <strong>${myDistrict.name}</strong>?`)) {
                // Remove this.id from the list
                Editor.removeCharacter(this, myDistrict);
                // Animate removal
                $result.slideUp(400, Editor.rerenderAndSave);
            }
        });
        $header.append($delete);
        $result.append($header);

        let $body = $(`
          <div class="message-body">
	    <article class="media">
	      <div class="media-left">
	  	<figure class="image is-96x96">
	  	  <img src="${this.image_url}" alt="Image">
	  	</figure>
	      </div>
	      <div class="media-content">
	      </div>
	    </article>
	  </div>`);
        $body.find("img")
            .attr("src", this.image_url)
            .click(() => {
                Dialogs.getImageUrl(this.image_url).then(newUrl => {
                    if (newUrl) {
                        this.image_url = newUrl;
                        Editor.rerenderAndSave();
                    }
                });
            });

        let $content = $body.find(".media-content");
        $content.append(`
<div class="field">
  <label class="label">Age</label>
  <div class="control">
    <input type="number" class="input">
  </div>
</div>`);
        let $ageField =
            $content.find("input[type='number']").val(this.age);
        $ageField.change(() => {
            this.age = $ageField.val();
        });
        $content.append(
            $(`<div class="field"><label class="label">Gender</label></div>`)
                .append(this.createGenderControl())
        );
        $content.append(`<label class="label">Traits</label>`);
        $content.append(this.createTraitsControl());
        $result.append($body);

        return $result;
    }
}

export class District implements IDistrict {
    id: number;
    name: string;
    color: BulmaColor;
    characters: number[];

    constructor(source: IDistrict = null) {
        if (source) {
            Object.keys(source).forEach(key => this[key] = source[key]);
        }
    }

    toElement(): JQuery {
        let $result = $(`
<div class="column is-half-desktop">
  <div class="message ${this.color}" data-district-id="${this.id}">
    <div class="message-header">
      <p>${this.name} <span class="icon is-small"><i class="fas fa-cog" title="Edit district..."></i></span></p>
      <button class="delete"></button>
    </div>
    <div class="message-body has-background-white">
    </div>
  </div>
</div>`);
        $result.find("i").click(() => {
            // Open district settings dialog
            Dialogs.showDistrictSettings(this).then(result => {
                if (result) {
                    this.name = result.name;
                    this.color = result.color;
                    Editor.rerenderAndSave();
                }
            });
        });
        $result.find(".delete").click(async () => {
            if (await Dialogs.confirm(`Really delete <strong>${this.name}</strong>?`, "Delete district", BulmaColor.Danger)) {
                Editor.removeDistrict(this);
                $result.fadeOut(400, Editor.rerenderAndSave);
            }
        });

        let $messageBody = $result.find(".message-body");
        Editor.currentChars
            .filter(c => this.characters.includes(c.id))
            .forEach(c => $messageBody.append(c.toElement()));
        let $addCharacter = $(`
<div class="message ${this.color}">
  <div class="message-header">
    <p>Add character</p>
  </div>
  <div class="message-body has-text-centered">
    <button class="button ${this.color} is-large" data-action="search">
      <span class="icon is-large"><i class="fas fa-2x fa-search-plus"></i></span>
    </button>
    <button class="button ${this.color} is-large" data-action="add">
      <span class="icon is-large"><i class="fas fa-2x fa-plus"></i></span>
    </button>
  </div>
</div>`);
        $addCharacter.find("[data-action='search']").click(() => {
            Dialogs.chooseCharacter(Editor.currentChars.map(c => c.id)).then(char => {
                if (char) {
                    let character = new Character(char);
                    Editor.currentChars.push(character);
                    this.characters.push(character.id);
                    character.toElement().hide()
                        .insertBefore($addCharacter)
                        .slideDown(400, Editor.rerenderAndSave);
                }
            });
        });
        $addCharacter.find("[data-action='add']").click(() => {
            let newId = Storage.getNextCharacterId();
            let newChar = new Character({
                id: newId,
                name: "",
                age: 18,
                gender: Gender.Male,
                image_url: "img/no-image.png",
                traits: []
            });
            Editor.currentChars.push(newChar);
            Storage.saveNewCharacter(newChar);
            this.characters.push(newId);
            newChar.toElement().hide()
                .insertBefore($addCharacter)
                .slideDown(400, Editor.rerenderAndSave);
        });
        $messageBody.append($addCharacter);
        return $result;
    }
}
